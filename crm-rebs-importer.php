<?php
/**
 * Plugin Name: CRM REBS Importer
 * Plugin URI: http://crm.rebs-group.com
 * Description: Plugin that handles importing properties from CRM REBS
 * Version: 0.1
 * Author: Felix Kerekes
 * Author URI: http://crm.rebs-group.com
 * License:
 */

if(!class_exists('CrmRebsImporter'))
{
    class CrmRebsImporter
    {
        public static function get_value_arrays() {
          return array(
            'property_type' => array(
              1 => 'Apartament',
              3 => 'Casă / Vilă',
              4 => 'Spațiu de birou',
              5 => 'Spațiu comercial',
              6 => 'Teren',
              7 => 'Spațiu industrial',
            ),
          );
        }

        /**
         * Construct the plugin object
         */
        public function __construct()
        {
          add_action('crm_rebs_check_property', array($this, 'check_property'));
          add_action('crm_rebs_update_properties', array('CrmRebsImporter', 'update_properties'));
        } // END public function __construct

        /**
         * Activate the plugin
         */
        public static function activate()
        {
          wp_schedule_event( time(), 'hourly', 'crm_rebs_update_properties' );
        }

        /**
         * Deactivate the plugin
         */
        public static function deactivate()
        {
          wp_clear_scheduled_hook('crm_rebs_update_properties');
        }

        public static function add_admin_settings() {
          add_settings_section(
            'crm_rebs_settings_section',
            'CRM REBS Settings',
            'crm_rebs_settings_callback'
          );
        }

        /**
         * Update all properties from the CRM REBS API.
         */
        public function update_properties()
        {
          error_log('Updating all properties...');

          $api_endpoint = get_option('api_endpoint');

          $options = get_option('crm_rebs_options');
          $api_base_domain = $options['base_domain'];

          $schema_url = '/api/public/property/schema/';
          $schema_response = wp_remote_get($api_base_domain . $schema_url);
          $schema = json_decode($schema_response['body'], true);

          $next = '/api/public/property/';

          do
          {
            // Fetch and deserialize the current page
            $response = wp_remote_get($api_base_domain . $next);
            $data = json_decode($response['body'], true);

            foreach ($data['objects'] as $property)
            {
              // The `crm_rebs_update_property` filter will add/update a post
              // for the property, and return the WP post ID as `wp_id`.
              $property = apply_filters( 'crm_rebs_update_property', $property);

              // Verify that a WP post for this property exists
              if (!array_key_exists('wp_id', $property)) {
                error_log("No `wp_id` found for property CP" . $property['id']);
                continue;
              }
              $post_id = $property['wp_id'];

              // Populate the post with meta information from the API
              foreach ($property as  $key => $value) {
                // Skip null / empty values
                if (!$value) {
                  continue;
                }

                // Check if this field accepts choices, according to the API schema
                if (array_key_exists($key, $schema['fields']) && array_key_exists('choices', $schema['fields'][$key])) {
                  $display_value = null;

                  // Choices are represented as an array of arrays containing key and value.
                  //
                  // i.e.
                  // [
                  //   [0, "Apartament"],
                  //   [1, "Casa / Vila"],
                  //   ...
                  // ]
                  foreach ($schema['fields'][$key]['choices'] as $choice) {
                    if ($value == $choice[0]) {
                      $display_value = $choice[1];
                    }
                  }

                  // Save the original int value in the meta
                  update_post_meta($post_id, $key, $value);

                  // Also save the the display value as a meta
                  if ($display_value) {
                    update_post_meta($post_id, $key . "_display", $display_value);
                  }
                } else if (is_string($value) || is_integer($value) || is_bool($value)) {
                  // Save any non-complex information as meta
                  update_post_meta($post_id, $key, $value);
                }

              }
            }

            // Fetch the next page, if exists
            $next = $data['meta']['next'];
          } while ($next);
        }

        /**
         * Check if a property is still available in the CRM REBS API.
         *
         * If it's not, the property is deleted.
         */
        public function check_property($post_id) {
          $crm_rebs_id = get_post_meta($post_id, 'crm_rebs_id', true);

          $options = get_option('crm_rebs_options');
          $api_base_domain = $options['base_domain'];

          $url = $api_base_domain . '/api/public/property/' . $crm_rebs_id;
          $response = wp_remote_get($url);
          if ($response['response']['code'] == 404) {
            wp_delete_post($post_id);
          }
        }

    } // END class CrmRebsImporter
} // END if(!class_exists('CrmRebsImporter'))

class CrmRebsSettings
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'CRM REBS Importer',
            'CRM REBS Importer Settings',
            'manage_options',
            'crm-rebs-importer-settings',
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'crm_rebs_options' );
        ?>
        <div class="wrap">
            <h2>CRM REBS Importer Settings</h2>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'crm_rebs_option_group' );
                do_settings_sections( 'crm-rebs-importer-settings' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'crm_rebs_option_group', // Option group
            'crm_rebs_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Import Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'crm-rebs-importer-settings' // Page
        );

        add_settings_field(
            'base_domain',
            'CRM Base Domain',
            array( $this, 'base_domain_callback' ),
            'crm-rebs-importer-settings',
            'setting_section_id'
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['base_domain'] ) )
            $new_input['base_domain'] = sanitize_text_field( $input['base_domain'] );

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function base_domain_callback()
    {
        printf(
            '<input type="text" id="base_domain" name="crm_rebs_options[base_domain]" value="%s" />',
            isset( $this->options['base_domain'] ) ? esc_attr( $this->options['base_domain']) : ''
        );
    }
}

if( is_admin() ) {
    $crm_rebs_settings = new CrmRebsSettings();
}

if(class_exists('CrmRebsImporter'))
{
  // Installation and uninstallation hooks
  register_activation_hook(__FILE__, array('CrmRebsImporter', 'activate'));
  register_deactivation_hook(__FILE__, array('CrmRebsImporter', 'deactivate'));

  // instantiate the plugin class
  $crm_rebs_importer = new CrmRebsImporter();

  // Uncomment this to force a property update
  //add_action('init', array('CrmRebsImporter', 'update_properties'));
}

?>
